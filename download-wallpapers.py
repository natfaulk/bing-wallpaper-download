PICTURE_DIR = ""
RESOLUTION = "1920x1080"
NUMBER_OF_IMAGES = 8 #up to maximum 8?

JSON_QUERY = "http://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n="

import urllib
import urllib2
import json
import os

if PICTURE_DIR == "":
    PICTURE_DIR = os.path.join(os.path.expanduser("~"),"Pictures","BingWallpapers")

if not os.path.exists(PICTURE_DIR):
        os.makedirs(PICTURE_DIR)

response = urllib2.urlopen(JSON_QUERY+str(NUMBER_OF_IMAGES))
data = json.load(response)
for i in data["images"]:
    filename = i["urlbase"][i["urlbase"].find('.') + 1:]+"_"+RESOLUTION+".jpg"
    if os.path.exists(PICTURE_DIR+"/"+filename):
        print "Not downloaded, picture already exists"
    else:
        print "Downloading %s" %filename
        urllib.urlretrieve("http://www.bing.com"+i["urlbase"]+"_"+RESOLUTION+".jpg",PICTURE_DIR+"/"+filename)

# Bing Wallpaper Download Script For Mac OSX
## (Also probably works on linux but not tested)

## About

I wanted a script that could be run automatically (like as a cron job) to download the Bing background images to a folder. The desktop background can then be set from this folder. The scripts I found were not very easy to customise and the official tool is only available for windows, so I wrote my own.

## How to install / use

Download the files

Either

1. Clone the repository using git

    ```
    git clone https://natfaulk@bitbucket.org/natfaulk/bing-wallpaper-download.git
    ```

2. Download all the files as a zip and extract.
    (go to Downloads in the sidebar, then Download repository)

### Configure the script
Open download-wallpapers.py in a text editor
To change where the photos are saved to, add a file path within the quotes of the first line (`PICTURE_DIR = ""`).
If this line is left as it is, the photos are stored in the default location of $HOME/Pictures/BingWallpapers

The resolution can also be changed in the same way. The maximum resolution images that bing provides are 1920x1200, however these have a bing watermark in the lower right corner. The maximum size without the watermark is 1920x1080 - the default resolution.

Resolutions that are known to work are:

* 1920x1200
* 1920x1080
* 1366x768
* 1024x768
* 800x600
* 800x480

Other resolutions may work

### Set the script to automatically run

There are several way to do this such as using a cron job, launchd or an ical event

#### Cronjob automation example
Open terminal and enter the following command:
```
env EDITOR=nano crontab -e
```

Add in the following line where /path/to/your/script.py is the path to the script. An easy way to do this is to drag the file into the terminal window.

```
MAILTO=""
0 * * * *  python /path/to/your/script.py
```

This causes the script to run hourly on the hour. The `MAILTO=""` is to stop terminal notifying you every time the script runs.

Press `Ctrl-x` then `y` then `Enter` to save and exit the file

### Set the desktop background
Finally set the desktop background by right clicking the desktop, `Change Desktop Background...`

Click the `+` under the folders and add the folder where the backgrounds are downloading to.
